# Weibaes

Weiabes is a p2p, decentralized, turn-based, strategy game similar to other popular games such as Pokemon.

The goal of Weibaes is to capture, train, and battle your Weibaes to gain prestige within the Weibae community

and therefore, the world.

Peer to Peer Battles
--------------------
Weibaes uses a p2p battle system built on top of Ethereum's whisper protocol to establish communication

between two peers. All battles are fought, in full, away from the Ethereum network; only the result of

the battle should be recorded on the Ethereum blockchain. This has two major benefits.


1. reduced gas fees 
2. fluid battles 
